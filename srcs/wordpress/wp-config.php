<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TF=_U06j_bzL}>f+D^9i,uJ}XNky,76 /sglp|ha/6v~94{gC+9D!Sg)ywLa|Op}' );
define( 'SECURE_AUTH_KEY',  '%zjmVEL%5l2hgNZNBtMI)_W/VlWT5G9$kci;?.mGG&7ZF+7/0X)&Q]P{n@rWe!Ne' );
define( 'LOGGED_IN_KEY',    'R8GHfzOcIw)Crp?{@_U%67E5~7j}_f_?ddGKOMb[9xx*_^>;Dvmiec:UKJ^PxW}8' );
define( 'NONCE_KEY',        '-?%;3(>6tP;tlpg/?YGl yv/|Ur~ZM==0Da ^okXxV1ydz_YQ}da ~> 9Yzs@$_0' );
define( 'AUTH_SALT',        'd-#LqA%ys>Vic%9Ywlb`LehKVqOR;jlpsr-_>?,Wun1!;NPJf%dfaF2745 V9jk0' );
define( 'SECURE_AUTH_SALT', 'm5tMP#:o0{*YmwA,ja@92)`6r_`t 8.U2r-ig!,ev=W@?kwLi9!4Tq.$_*7d:N07' );
define( 'LOGGED_IN_SALT',   ']Wr.r`A ^Tt$&G,+K{7@vhWRE|47Ee1m~U(0*9X-1p]Fq:&}L^d,.:DT.y[ 5z,u' );
define( 'NONCE_SALT',       'Qy[`~>M24$XU_Wc!(e,e8o[Q%YH_Na$=q][VdW*>l6(G~9Uox s)}6SIC<qa?vJ<' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
