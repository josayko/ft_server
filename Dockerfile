############################################################
# Dockerfile to build Nginx Installed Containers
# Based on Debian Buster
############################################################

# Parent image
FROM debian:buster-slim

LABEL description="Minimal Nginx webserver with Debian Buster"
MAINTAINER Jonny Saykosy <josaykos@student.42.fr>

# Update the packages repository
# and install necessary tools
RUN apt-get update \
	&& apt-get install -y nano wget

# Download and install Nginx
RUN apt-get install -y nginx

# Download and install Openssl
RUN apt-get install -y openssl

# Download and install mySQL / MariaDB
RUN apt-get install -y default-mysql-server

# Create mySQL user
RUN /etc/init.d/mysql start && \
	mysql -e "CREATE DATABASE wordpress;" && \
	mysql -e "CREATE USER 'admin'@'localhost' IDENTIFIED BY '123'" && \
	mysql -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost'" && \
	mysql -e "FLUSH PRIVILEGES"

# Download and install PHP and PHP modules
RUN apt-get -y install php7.3-fpm php7.3-common php7.3-mysql \
	php7.3-gmp php7.3-curl php7.3-intl php7.3-mbstring php7.3-xmlrpc \
	php7.3-gd php7.3-xml php7.3-cli php7.3-zip php7.3-soap php7.3-imap

# Add some configuration files from local srcs folder to container
COPY /srcs/php/info.php /var/www/html/
COPY /srcs/nginx/default /etc/nginx/sites-available/
COPY /srcs/certificates/localhost.crt /etc/ssl/certs/localhost.crt
COPY /srcs/certificates/localhost.key /etc/ssl/private/localhost.key

# Link Nginx to Wordpress site
# RUN ln -s /etc/nginx/sites-available/wordpress.conf  /etc/nginx/sites-enabled/

# Download, install and configure Wordpress
COPY /srcs/wordpress/. /var/www/html/wordpress/
RUN chown www-data: /var/www/html/wordpress/ -R
COPY /srcs/mysql/wp_db.sql .
RUN /etc/init.d/mysql start && \
	mysql wordpress < wp_db.sql

# Download, install and configure PHPmyadmin
RUN wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-english.tar.gz
RUN mkdir /var/www/html/wordpress/phpmyadmin
RUN tar xzf phpMyAdmin-4.9.0.1-english.tar.gz --strip-components=1 -C /var/www/html/wordpress/phpmyadmin
COPY /srcs/phpmyadmin/config.inc.php /var/www/html/wordpress/phpmyadmin/
RUN rm phpMyAdmin-4.9.0.1-english.tar.gz

# Expose ports
EXPOSE 80 443

# Set the default command to execute
# when creating new container
CMD /etc/init.d/mysql start \
	&& /etc/init.d/php7.3-fpm start \
	&& nginx -g 'daemon off;' \
	&& /bin/bash
